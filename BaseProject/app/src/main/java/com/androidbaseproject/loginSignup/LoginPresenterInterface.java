package com.androidbaseproject.loginSignup;
/**
 * Created by netset on 31/8/18.
 */

public interface LoginPresenterInterface {

    void checkValidation(String email, String password);
}
