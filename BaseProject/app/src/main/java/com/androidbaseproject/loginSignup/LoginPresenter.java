package com.androidbaseproject.loginSignup;
import android.util.Patterns;

/**
 * Created by netset on 31/8/18.
 */

public class LoginPresenter implements LoginPresenterInterface{
    LoginView loginView;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }
    @Override
    public void checkValidation(String email, String password) {
        if (email.isEmpty()) {
            loginView.checkValidationError("Enter the email address");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginView.checkValidationError("Enter valid email address");
        } else if (password.isEmpty()) {
            loginView.checkValidationError("Enter your password");
        }else {
            hitLoginApi(email,password);
        }
    }

    public void hitLoginApi(String email,String pass){

        loginView.success("");


    }
}
