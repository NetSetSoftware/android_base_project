package com.androidbaseproject.loginSignup;

/**
 * Created by netset on 31/8/18.
 */

public interface LoginView {

    void checkValidationError(String s);

    void success(String s);
}
