package com.androidbaseproject.loginSignup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidbaseproject.BuildConfig;
import com.androidbaseproject.R;


public class LoginFragment extends Fragment implements LoginView {

    EditText emailET;
    EditText passwordET;
    Button loginBT;
    TextView registerTV;
    TextView forgetTV;
    LoginPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        // unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new LoginPresenter(this); //TODO: create presenter reference (Create Object)
        emailET = view.findViewById(R.id.emailET);
        passwordET = view.findViewById(R.id.passwordET);
        loginBT = view.findViewById(R.id.loginBT);

        if (BuildConfig.DEBUG) {
            emailET.setText("abc@gmail.com");
            passwordET.setText("123456");
        }

        loginBT = view.findViewById(R.id.loginBT);
        loginBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: call validation method to presenter class
                presenter.checkValidation(emailET.getText().toString(), passwordET.getText().toString());

            }
        });
    }

    public void goToLogin() {

        //TODO: Go to next fragment and activity
/*
        Intent loginIntent = new Intent(getActivity(), HomeActivity.class);
        getActivity().startActivity(loginIntent);
        getActivity().finish();
*/

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // unbinder.unbind();
    }

    @Override
    public void checkValidationError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void success(String s) {
        goToLogin();
    }
}
